import React, { useState, useEffect } from "react";
import "./AllProductPage.scss"
import Product from "../../components/Product.jsx";
import ModalText from "../../components/Modal/ModalText.jsx";
import ModalImage from "../../components/Modal/ModalImage.jsx";
import { useDispatch, useSelector } from "react-redux"
import { actionFetchData, actionAddFavorite, actionAddBasket } from "../../store/actions.js"
import { selectorAllProducts } from "../../store/selectors.js"

const AllProductPage = () => {
    const [isModalText, setIsModalText] = useState(false);
    const [isModalImage, setIsModalImage] = useState(false);
    const [currentDate, setCurrentDate] = useState({});

    const cards = useSelector(selectorAllProducts)
    
    const dispatch = useDispatch();

    const handleCurrentData = (item) => {
        setCurrentDate(item)
    }

    const handleModalText = () => setIsModalText(!isModalText)
    const handleModalImage = () => setIsModalImage(!isModalImage)

    const handleAddFavorite = (data) => {
        dispatch(actionAddFavorite(data))
    }

    const handleAddBasket = (data) => {
        dispatch(actionAddBasket(data))
    }

    useEffect(() => {
        dispatch(actionFetchData())
    }, []);

    return (
        <>
            <div className="products-box">
                {cards.map((item, index) => {
                    return <Product onClick={() => {
                        handleCurrentData(item)
                    }} key={index} data={item} onText={handleModalText} onImg={handleModalImage} />
                })}

            </div>
            {isModalImage && <ModalImage close={handleModalImage}
                addBasket={() => 
                    handleAddBasket(currentDate)
                }
                name={currentDate.name}
                color={currentDate.color}
                price={currentDate.price}
                image={currentDate.image}
            />}
            {isModalText && <ModalText close={handleModalText}
                addFavorite={() => {
                    handleAddFavorite(currentDate)
                }}
                name={currentDate.name}
                color={currentDate.color}
                price={currentDate.price}
            />}
        </>
    )
}

export default AllProductPage;