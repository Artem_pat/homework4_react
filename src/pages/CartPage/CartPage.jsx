import React, { useEffect, useState } from "react";
import CartItem from "../../components/Items/CartItem";
import "./CartPage.scss"
import ModalDel from "../../components/Modal/ModalDel";
import { useSelector, useDispatch } from "react-redux";
import { actionRemoveBasket, actionAddFavorite } from "../../store/actions";
import { selectorBasket } from "../../store/selectors"

const CartPage = () => {

    const [isModal, setIsModal] = useState(false)
    const [currentDate, setCurrentDate] = useState({})

    const dispatch = useDispatch()
    const basket = useSelector(selectorBasket) 

    const delBasketItem = (item) => {
        dispatch(actionRemoveBasket(item))
    }

    const handleAddFavorite = (item) => {
        dispatch(actionAddFavorite(item))
    }

    const delModal = () => {
        setIsModal(!isModal)
    }

    const handleCurrentData = (item) => {
        return setCurrentDate(item)
    }

    return (
        <>
            <h2 className="cart-title">Cart List</h2>
            {basket.map((item, index) => {

                return <CartItem key={index} data={item} onCloseModal={() => {
                    delModal()
                    handleCurrentData(item)
                }} onAddBasket={
                    () => {
                        handleAddFavorite(item)
                        delBasketItem(item)
                    }
                } />
            })}
            {isModal && <ModalDel
                del={() => delBasketItem(currentDate)}
                close={delModal}
                name={currentDate.name}
                color={currentDate.color}
                price={currentDate.price}
                image={currentDate.image}
            />}
        </>
    )
}

export default CartPage;