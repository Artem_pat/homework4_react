import React from "react";
import ModalWrapper from "./ModalWrapper";
import ModalBody from "./ModalBody";
import ModalClose from "./ModalClouse";
import ModalHeader from "./ModalHeader";
import ModalContent from "./ModalContent";
import ModalFooter from "./ModalFooter";

const ModalText = ({ close, addFavorite, name, color, price }) => {
    return (
        <ModalWrapper onClick={close}>
            <ModalBody className="modal">
                <ModalClose onClick={() => {
                    close()
                    
                }} />
                <ModalHeader>{name}</ModalHeader>
                <ModalContent>
                    <p>колір: {color}</p>
                    <p>ціна: {price}грн</p>
                </ModalContent>
                <ModalFooter firstClick={() => {
                    addFavorite()
                    close()

                }} firstText="add to favorite" />
            </ModalBody>
        </ModalWrapper>
    )
}

export default ModalText;