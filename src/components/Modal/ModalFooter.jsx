import React from "react";
import Button from "../Button/Button"

const ModalFooter = (props) => {
    const {firstText, secondaryText, firstClick, secondaryClick} = props
    return (
        <div className={secondaryText ? "modal-footer" : "modal-footer-onetbn"}>
            {firstText && firstClick && (
                <Button className="btn btn__bg-purpl" onClick={firstClick}>{firstText}</Button>  
            )}
            {secondaryText && secondaryClick && (
                <Button className="btn btn__text-purpl" onClick={secondaryClick}>{secondaryText}</Button>  
            )}
        </div>
    )
}

export default ModalFooter;