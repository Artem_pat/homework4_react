import React from "react";

const ModalWrapper = (props) => {

    return (
        <div className="modal-wrapper" onClick={(e) => {
            if (e.target.classList.contains("modal-wrapper")) {
              props.onClick();
            }
        }}>
            {props.children}
        </div>
    )
}

export default ModalWrapper;