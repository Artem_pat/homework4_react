import React from "react";
import { Link } from "react-router-dom"
import PropTypes from "prop-types"
import Basket from "../../assets/icons/basket1.svg?react"
import Like from "../../assets/icons/like.svg?react"
import "./HeaderSection.scss"
import LikeCount from "./Counts/LikeCount";
import BasketCount from "./Counts/BasketCount";

const HeaderSection = () => {
    
    return (
        <>
            <header className="header-title">
                <Link to="/"><h2 className="header-title__text">Categories For Men</h2></Link>
                <div className="header-title__icon-box">
                    <Link to="/basket"><Basket className="header-title__basket" /></Link>
                    <Link to="/favorite"><Like className="header-title__like" /></Link>
                    <LikeCount/>
                    <BasketCount/>
                </div>
            </header>
        </>
    )
}


export default HeaderSection