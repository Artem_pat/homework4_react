import React from "react";
import "./LikeCount.scss"
import {useSelector} from "react-redux"

const LikeCount = () => {
    const count = useSelector((state) => state.favorite.length)

    return(
        <span className="like-count">{count}</span>
    )
}

export default LikeCount;