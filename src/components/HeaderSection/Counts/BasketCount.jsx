import React from "react";
import PropTypes from "prop-types"
import "./BasketCount.scss"
import { useSelector } from "react-redux"

const BasketCount = () => {
    const count = useSelector((state) => state.basket.length)
    return(
        <span className="basket-count">{count}</span>
    )
}

export default BasketCount;