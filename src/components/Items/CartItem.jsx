import React from "react";
import DelIcon from "../../assets/icons/deleteicon.svg?react"
import "./Items.scss"
import Button from "../Button/Button";
import { useDispatch } from "react-redux"
import { actionAddFavorite, actionRemoveFavorite } from "../../store/actions"

const CartItem = ({ data, onCloseModal, onAddBasket }) => {
    const dispatch = useDispatch();
    const handleAddBasket = (item) => {
        dispatch(actionAddFavorite(item))
    }
    const handleRemoveFavorite = (item) => {
        dispatch(actionRemoveFavorite(item))
    }

    const { image,
        name,
        price,
        article,
        color,
        fill } = data
    return (
        <div className="item-box cart-box">
            <img className="item-img" src={`images/${image}`} alt={name} />
            <div className="item-info">
                <h2 className="item-info__title">{name}</h2>
                <p className="item-info__text item-info__text--md">article: {article}</p>
                <p className="item-info__text">color: {color}</p>
            </div>
            <Button onClick={() => {
                onAddBasket()
            }}>Add To Favorite</Button>
            <p className="item-price">{price} грн</p>
            <DelIcon className="item-del" onClick={onCloseModal} />
        </div>
    )
}

export default CartItem;