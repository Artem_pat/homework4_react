import { createAction, createAsyncThunk } from "@reduxjs/toolkit"
import { sendRequest } from "../helpers/sendRequest"

export const actionFetchData = createAsyncThunk(
    "fetchData",
    async () => {
        const saveProducts = JSON.parse(localStorage.getItem("products"));
        if (saveProducts) {
            return saveProducts
        } else {
            const data = await sendRequest("/index.json");
            return data.map((product) => {
                return {...product, isFavorite: false, isBasket: false, basketCounter: 0}
            })
        }
    }
)

export const actionAddFavorite = createAction("ACTION_ADD_FAVORITE")
export const actionRemoveFavorite = createAction("ACTION_REMOVE_FAVORITE")
export const actionAddBasket = createAction("ACTION_ADD_BASKET")
export const actionRemoveBasket = createAction("ACTION_REMOVE_BASKET")
export const actionActiveLike = createAction("ACTION_ACTIVE_LIKE")
export const actionActiveBasket = createAction("ACTION_ACTIVE_BASKET")